import { defineConfig } from 'vite';
import fs from 'fs/promises';
import path from 'path';
import { fileURLToPath } from 'url';
import { glob } from 'glob';

function copyAndMinifyPlugin(): Plugin {
  return {
    name: 'copy-and-minify',
    async writeBundle(options: OutputOptions, bundle: Record<string, any>) {
      const srcDir = 'src/dsfr';
      const distDir = 'src/public/dsfr';

      async function copyRecursiveSync(src: string, dest: string) {
        const exists = await fs.stat(src).then(() => true).catch(() => false);
        if (!exists) return;

        const stats = await fs.stat(src);
        const isDirectory = stats.isDirectory();

        if (isDirectory) {
          await fs.mkdir(dest, { recursive: true });
          const children = await fs.readdir(src);
          for (const childItemName of children) {
            await copyRecursiveSync(path.join(src, childItemName), path.join(dest, childItemName));
          }
        } else {
          const ext = path.extname(src).toLowerCase();
          if (ext !== '.js' && ext !== '.css') {
            if (['.woff', '.woff2'].includes(ext)) {
              const fontsDest = path.join(distDir, 'dsfr/fonts', path.basename(src));
              await fs.mkdir(path.dirname(fontsDest), { recursive: true });
              await fs.copyFile(src, fontsDest);
            } else {
              await fs.copyFile(src, dest);
            }
          }
        }
      }

      await copyRecursiveSync(srcDir, distDir);
    }
  };
}

export default defineConfig({
  base: '',
  assetsInclude: ['**/*.svg', '**/*.woff', '**/*.woff2'],
  build: {
    outDir: 'src/public/dsfr',
    emptyOutDir: true,
    minify: 'terser',
    cssMinify: true,
    rollupOptions: {
      input: Object.fromEntries(
        glob.sync('src/dsfr/**/*.{js,ts,css}').map(file => [
          path.relative('src/dsfr/', file),
          fileURLToPath(new URL(file, import.meta.url))
        ])
      ),
      output: {
        entryFileNames: (chunkInfo) => {
          const ext = path.extname(chunkInfo.name);
          const dir = path.dirname(chunkInfo.name);
          const name = path.basename(chunkInfo.name, ext);
          if (ext === '.js') {
            return path.join(dir, `${name}.min${ext}`);
          }
          return path.join(dir, `${name}${ext}`);
        },
        chunkFileNames: (chunkInfo) => {
          const ext = path.extname(chunkInfo.name);
          const dir = path.dirname(chunkInfo.name);
          const name = path.basename(chunkInfo.name, ext);
          return path.join(dir, `${name}.min${ext}`);
        },
        assetFileNames: (assetInfo) => {
          const ext = path.extname(assetInfo.name);
          const dir = path.dirname(assetInfo.name);
          const name = path.basename(assetInfo.name, ext);
          if (ext === '.css') {
            return path.join(dir, `${name}.min${ext}`);
          }
          if (['.woff', '.woff2'].includes(ext)) {
            return `fonts/${name}${ext}`;
          }
          return path.join(dir, `${name}${ext}`);
        },
      },
    },
  },
  plugins: [
    copyAndMinifyPlugin(),
    {
      name: 'adjust-css-font-paths',
      transform(code: string, id: string) {
        if (id.endsWith('.css')) {
          return {
            code: code
              .replace(/url\((['"]?)(.+?\.woff2?)(['"]?)\)/g, 'url(fonts/$2$3)')
              .replace(/url\((['"]?)favicon\/manifest\.webmanifest(['"]?)\)/g, 'url($1favicon/manifest.webmanifest$2)'),
            map: null
          };
        }
      }
    }
  ],
});