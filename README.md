# DSFR

DSFR is a base theme for Drupal, which uses the DSFR library (FRench Design System, in french: "Design System FRançais" or "Système de Design de l'État").

See: [official DSFR library documentation](https://www.systeme-de-design.gouv.fr/)

French version of this document: ./docs/fr/LISEZMOI.md

## Scope of use

The DSFR library is to be used for digital projects within the French State: central administrations, their departments, interministerial delegations, prefectures, embassies and all decentralized services.

More details: [Scope of DSFR use](https://www.systeme-de-design.gouv.fr/comment-utiliser-le-dsfr/perimetre-d-application-du-dsfr) (available only in French)

## Library version

This is indicated in the package.json file and the dsfr.libraries.yml file!

## About 

This Drupal theme and the modules that revolve around it have been developed by the Interacademic Information Systems Service of the Auvergne-Rhône-Alpes academic region ("Service interacadémique des systèmes d'information de la région académique Auvergne-Rhône-Alpes" or SIASI-AURA).
This open-source DSFR-Drupal ecosystem is monitored by the interministerial digital department (DINUM).

## Theme settings

From the “/admin/appearance/settings/dsfr” page, you can among other things:

1. Change the institution name
2. Activate mourning mode
3. Add a badge (e.g. “preview” in the color of your choice)
4. Customize logo (in header and footer)
5. Manage margins of main theme areas (header, footer, etc.)
6. Enable support for optional modules RGPD (Tacjs) and newsletter (Simplenews)
7. Select the parts of the library to be loaded

## Detailed documentation

See: ./docs/en/

## Annotations

1. See : 
* https://www.drupal.org/project/dsfr_twig_components
* https://www.drupal.org/project/dsfr_core
* https://www.drupal.org/project/dsfr_menu
* https://www.drupal.org/project/dsfr_paragraph
2. https://www.drupal.org/project/dsfr_kickstart