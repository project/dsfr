The scripts in this folder are designed to automate certain functions via your shell. Be sure to check your write access rights.

For example, you can quickly create your own ready-to-use DSFR child themes: see "docs/en/create_childtheme.md" (or "docs/fr/creer_theme_enfants.md")