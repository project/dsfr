#!/bin/bash
LC_CTYPE=en_US.utf8

echo "
#----------------------------------------------------------------------------#
#
# Script to quickly create a DSFR child theme for Drupal ^10 || ^11
# You should be in the theme/custom folder
#
# -- /!\ WARNING: 
#
# Please consult the document LICENSE.txt and verify that you are authorized 
# to use the French State Design System.
#
#----------------------------------------------------------------------------#
"
echo '
# Please enter a NEW EXCLUSIVE machine name of your child theme (required)
# /!\ Cannot be empty
# Only lowercase letters (a-z), numbers (0-9), and underscores (_) are allowed.
# The name must contain at least one letter, be at least 3 characters long, and cannot start with "_".
# e.g. france_2025:'
read DSFR_CHILD

# Delete all characters that are not letters, numbers or “_”.
DSFR_CHILD=$(echo "$DSFR_CHILD" | tr '[:upper:]' '[:lower:]' | tr -cd 'a-z0-9_')

# If the variable starts with a “_”, delete it
DSFR_CHILD=$(echo "$DSFR_CHILD" | sed 's/^_//')

# Vérifier si la variable est vide, contient moins de 3 caractères ou n'a pas de lettre
if [ -z "$DSFR_CHILD" ] || [ ${#DSFR_CHILD} -lt 3 ] || ! echo "$DSFR_CHILD" | grep -q '[a-z]'; then
  echo "ERROR - A child theme MACHINE name is mandatory, must contain only lowercase letters (a-z), numbers (0-9), underscores (_), must be at least 3 characters long, and must include at least one letter.
  "
  exit 1
fi

# Create a new variable with the name in uppercase
DSFR_CHILD_NAME=$(echo "$DSFR_CHILD" | tr '[:lower:]' '[:upper:]')

# Create a third variable where “_” is replaced by “-”.
DSFR_CHILD_DASHED=$(echo "$DSFR_CHILD" | sed 's/_/-/g')

echo "
Processed child theme name: $DSFR_CHILD"

echo '
#----------------------------------------------------------------------------#
# Please enter the name of your institution (not required)
# e.g. République <br> Française:'
read DSFR_CHILD_INSTITUTION

[ -d ../../custom ] || mkdir ../../custom
cp -r _dsfr_child ../../custom/$DSFR_CHILD
cd ../../custom/$DSFR_CHILD

find . -depth -name "*.starterkit" -exec sh -c 'mv "$1" "${1%.starterkit}.yml"' _ {} \;

for file in *_dsfr_child.*; do mv $file ${file//_dsfr_child/$DSFR_CHILD}; done
for file in */*/*_dsfr_child.*; do mv $file ${file//_dsfr_child/$DSFR_CHILD}; done
for file in */*/*\._dsfr_child_*; do mv $file ${file//_dsfr_child/$DSFR_CHILD}; done

for  file in config/*/*; do
  sed -i "s/_dsfr_child/$DSFR_CHILD/g" config/*/*.yml
done

if [[ $DSFR_CHILD_NAME ]]; then
  sed -i "s/DSFR Child Theme/$DSFR_CHILD_NAME/g" $DSFR_CHILD.info.yml
  sed -i "s/_dsfr_child/$DSFR_CHILD/g" $DSFR_CHILD.info.yml
  sed -i "s/_dsfr_child/$DSFR_CHILD/g" $DSFR_CHILD.libraries.yml
  sed -i "s/_dsfr_child/$DSFR_CHILD/g" src/script/$DSFR_CHILD.ts
  sed -i "s/dsfr-child/$DSFR_CHILD_DASHED/g" package.json
  sed -i "s/_dsfr_child/$DSFR_CHILD/g" vite.config.ts
fi;
if [[ $DSFR_CHILD_INSTITUTION ]]; then

  # https://www.tecmint.com/convert-files-to-utf-8-encoding-in-linux/
  for  file in config/*/*.yml; do
    sed -i "s/République <br> Française/$DSFR_CHILD_INSTITUTION/g" config/*/$DSFR_CHILD.*.yml
  done

fi;

echo "
#----------------------------------------------------------------------------#
# Preview of the '$DSFR_CHILD'.info.yml file
"
head -n 4 $DSFR_CHILD.info.yml
echo "
#----------------------------------------------------------------------------#
# Preview of $DSFR_CHILD.settings.yml and $DSFR_CHILD.schema.yml
"
head -n 2 config/install/$DSFR_CHILD.settings.yml

echo "
#----------------------------------------------------------------------------#

Your DSFR $DSFR_CHILD_NAME child theme is ready to use! ^_^ 
"