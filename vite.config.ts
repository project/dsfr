import { defineConfig } from 'vite';
import { resolve } from 'path';

const assets: string = '';
const script: string = 'src/script/';
const style: string = 'src/style/';

export default defineConfig({

  publicDir: resolve(__dirname, 'src/public'),
  build: {
    minify: 'terser',
    outDir: 'dist',
    emptyOutDir: true,
    sourcemap: false,
    cssCodeSplit: true,
    rollupOptions: {

      input: {

        // Scripts
        dsfr_settings: resolve(__dirname, script+'dsfr_settings.ts'),

        // Style
        dsfr_drupal: resolve(__dirname, style+'dsfr_drupal.css'),
        dsfr_drupal_admin: resolve(__dirname, style+'dsfr_drupal_admin.css'),
        dsfr_drupal_maintenance: resolve(__dirname, style+'dsfr_drupal_maintenance.css'),
        dsfr_drupal_maintenance_background: resolve(__dirname, style+'dsfr_drupal_maintenance_background.css'),
        dsfr_drupal_user: resolve(__dirname, style+'dsfr_drupal_user.css'),

        // Modules override
        dsfr_simplenews: resolve(__dirname, style+'dsfr_simplenews.css'),
        dsfr_tacjs: resolve(__dirname, style+'dsfr_tacjs.css'),
      
        // CSS Override Core Drupal
        'jquery-ui-components': resolve(__dirname, style+'admin/jquery-ui-components.css'),
      },

      output: {
        assetFileNames: (assetInfo) => {
          let extType = assetInfo.name.split('.').at(1);
          if (/\.(png|jpe?g|gif|svg|webp|webm|mp3)$/.test(assetInfo.name)) {
            return `${assets}img/${extType}/[name].${extType}`
          }
          if (/\.(css)$/.test(assetInfo.name)) {
            return `${assets}css/[name].min.${extType}`
          }
          if (/\.(woff|woff2|eot|ttf|otf)$/.test(assetInfo.name)) {
            return `${assets}fonts/[name].min.${extType}`
          }
        },
        chunkFileNames: 'js/[name].min.js',
        entryFileNames: 'js/[name].min.js',
      },
    }
  }
})
