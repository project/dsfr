<?php

namespace Drupal\dsfr\settings;

use Drupal\Core\Config\ImmutableConfig;

/**
 * Manages DSFR components.
 */
class Components {

  /**
   * Provides a list of component styles.
   *
   * @return string[] An array of component names.
   */
  public static function list(): array {

    return [
      'accordion',
      'alert',
      'badge',
      'breadcrumb',
      'button' ,
      'callout',
      'card',
      'checkbox',
      'connect',
      'consent',
      'content',
      'display',
      'download',
      'follow',
      'footer',
      'form',
      'header',
      'highlight',
      'input',
      'link',
      'logo',
      'modal',
      'navigation',
      'notice',
      'pagination',
      'password',
      'quote',
      'radio',
      'search',
      'segmented',
      'select',
      'share',
      'sidemenu',
      'skiplink',
      'stepper',
      'summary',
      'tab',
      'table',
      'tag',
      'tile',
      'toggle',
      'tooltip',
      'transcription',
      'translate',
      'upload',
      'utility', 
    ];    
  }

  /**
   * Initialize DSFR components based on theme settings and external library flag.
   *
   * @param ImmutableConfig $theme_setting The immutable configuration object containing theme settings.
   * @param int $external_lib Flag indicating whether to use external libraries (0 for internal, 1 for external).
   * 
   * @return array<string, mixed> An array containing initialized DSFR component information.
   *   The structure is:
   *   [
   *     'components' => [
   *       'component_name' => ['lib' => true],
   *       'enabled' => int (number of enabled components)
   *     ]
   *   ]
   */
  public static function initDsfrComponents(
    ImmutableConfig $theme_setting, 
    ?int $external_lib
  ): array {

    $dsfr['components'] = [];
    $components = self::list();

    if ( $external_lib != 1 ) {

      $enabledCount = 0;  
      foreach ($components as $component) {

        $setting_key = 'code.components.' . $component;
        if ( $theme_setting->get( $setting_key ) == 1 ) {

          $dsfr['components'][$component]['lib'] = true;
          $enabledCount++;
        }
      }

      $dsfr['components']['enabled'] = $enabledCount;
    } 

    return $dsfr;
  }
}