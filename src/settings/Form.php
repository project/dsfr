<?php

namespace Drupal\dsfr\settings;

use Drupal\Core\Config\ImmutableConfig;
use Drupal\dsfr\settings\Tools;

/**
 * Provides recurring templates for theme-related forms
 */
class Form {

  /**
   * Provides a details type form element.
   *
   * @param string $string The title of the details element.
   * @param string|null $description Optional description for the details element.
   * @return array The details form element array.
   */
  public static function detailsType( string $string, ?string $description = NULL ): array {

    $details = [
      '#type' => 'details',
      '#title' => Tools::t( $string ),
      '#group' => 'dsfr',
      '#tree' => TRUE
    ];

    if( $description != NULL) $checkbox['#description'] = $description;

    return $details;
  }

  /**
   * Creates a checkbox form element.
   *
   * @param string $title The title of the checkbox.
   * @param string $default The default value key in theme settings.
   * @param ImmutableConfig $theme_setting The theme settings configuration.
   * @param string|null $prefix Optional prefix HTML for the checkbox.
   * @param string|null $description Optional description for the checkbox.
   * @return array The checkbox form element array.
   */
  public static function checkboxType(
    string $title,
    string $default,
    ImmutableConfig $theme_setting, 
    ?string $prefix = NULL,
    ?string $description = NULL
  ): array {

    $checkbox = [
      '#type' => 'checkbox',
      '#title' => Tools::t( $title ),
      '#default_value' => $theme_setting->get( $default ),
    ];

    if( $prefix !== NULL ) $checkbox['#prefix'] = $prefix;
    if( $description !== NULL ) $checkbox['#description'] = $description;

    return $checkbox;
  }

  /**
   * Creates a margin pattern form element.
   *
   * @param string $section The section name for the margin pattern.
   * @param ImmutableConfig $theme_setting The theme settings configuration.
   * @param array $top The top margin settings [title, description, default].
   * @param array $bottom The bottom margin settings [title, description, default].
   * @return array The margin pattern form element array.
   */
  public static function marginPattern(
    string $section,
    ImmutableConfig $theme_setting, 
    array $top,
    array $bottom
  ): array {
    return [
      'top' => self::selectMargin($top[0], $top[1], $section.'.margin.top', $theme_setting, $top[2]),
      'bottom' => self::selectMargin($bottom[0], $bottom[1], $section.'.margin.bottom', $theme_setting, $bottom[2]),
    ];
  }

  /**
   * Creates a select form element for margin options.
   *
   * @param string $title The title of the select element.
   * @param string $description The description of the select element.
   * @param string $default The default value key in theme settings.
   * @param ImmutableConfig $theme_setting The theme settings configuration.
   * @param int|null $option Optional default margin option.
   * @return array The select form element array.
   */
  public static function selectMargin( 
    string $title,
    string $description,
    string $default,
    ImmutableConfig $theme_setting, 
    ?int $option = 3
  ): array {

    return [
      '#type' => 'select',
      '#title' => $title,
      '#description' => $description,
      '#default_value' => $theme_setting->get( $default ),
      '#options' => self::margin($option)
    ];
  }

  /**
   * Get margin options
   *
   * @param int $default
   * @return array<int|string, TranslatableMarkup|string>
   */
  public static function margin( int $default = 3 ): array {

    for( $i = 0; $i <= 32; $i++) {

      if ( $i == 0 ) { $options_margin[0] = Tools::t('None'); }
      elseif ( $i == 1 ) { $options_margin[1] = Tools::t('Minimal size'); } 
      else { $options_margin[$i] = $i . ' x ' . Tools::t('minimal size'); }

      if( $i == $default ) $options_margin[$i] .= ' (' . Tools::t('by default') . ')';
    }
    return $options_margin;
  }
}