<?php

namespace Drupal\dsfr\settings;

use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Manages module-related operations for DSFR.
 */
class Modules {

  /**
   * Get special modules and their status.
   *
   * @return array<string, int> 
   * An associative array where keys are module names 
   * and values are their status (1 for enabled, 0 for disabled).
   */
  public static function getModules(): array {

    // List of modules to check
    $modules = [
      'dsfr_core',
      'dsfr_menu',
      'dsfr_paragraph',
      'dsfr_twig_components',
      'dsfr_views',
      'search_api',
      'simple_styleguide'
    ];
    
    return self::checkModules( $modules );
  }

  /**
   * Checks the existence of each module in the given list.
   *
   * @param array<string> $modules An array of module names to check.
   * @return array<string, int> 
   * An associative array where keys are module names 
   * and values are their status (1 for exists, 0 for doesn't exist).
   */
  public static function checkModules(array $modules): array {

    /** @var ModuleHandlerInterface $moduleHandler */
    $moduleHandler = \Drupal::service('module_handler');
    $check_modules = [];

    foreach( $modules as $module ) { 
      $check_modules[$module] = ( $moduleHandler->moduleExists($module) ) ? 1 : 0; 
    }
    return $check_modules;
  }
}