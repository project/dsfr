<?php

namespace Drupal\dsfr\settings;

use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Url;
use Drupal\dsfr\settings\Components;
use Drupal\dsfr\settings\Tools;

class Theme {

  /**
   * Tabs for theme settings forms
   * @return array<string, mixed>
   */
  public static function settings(): array {

    // GET general information
    $https = (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https' && $_SERVER["REQUEST_SCHEME"] == 'http') ? 'https://' : 'http://';
    $domain = $https . $_SERVER['HTTP_HOST'];
    $root = \Drupal::request()->server->get('DOCUMENT_ROOT');

    $global = [
      'domain'   => $domain,
      'front'    => Url::fromRoute('<front>')->setAbsolute()->toString(),
      'https'    => $https,
      'root'     => $root
    ];

    // -------------------------------------------------------------------------------- //
    // GET DSFR Information
    $theme_handler = \Drupal::service('theme_handler');
    $theme_dsfr_path = '/' . $theme_handler->getTheme( 'dsfr' )->getPath();
    $theme_dsfr_absolute_path = $root . $theme_dsfr_path;
    $theme_dsfr_dist = $theme_dsfr_path . '/dist/';
    $theme_dsfr_absolute_dist = $theme_dsfr_absolute_path . '/dist/';
    $theme_dsfr_lib = $theme_dsfr_dist . 'dsfr/';
    $theme_dsfr_absolute_lib = $theme_dsfr_absolute_dist . 'dsfr/';
    $theme_dsfr_artwork = $theme_dsfr_lib . 'artwork/';
    $theme_dsfr_absolute_artwork = $theme_dsfr_absolute_lib . 'artwork/';
    $theme_dsfr_setting = \Drupal::config('dsfr.settings');

    $dsfr_theme = [
      'name'                => 'dsfr',
      'dashboard'           => Url::fromRoute('system.theme_settings_theme', ['theme' => 'dsfr'])->toString(),
      'path'                => $theme_dsfr_path,
      'absolute_path'       => $theme_dsfr_absolute_path,
      'dist'                => $theme_dsfr_dist,
      'absolute_dist'       => $theme_dsfr_absolute_dist,
      'lib'                 => $theme_dsfr_lib,
      'absolute_lib'        => $theme_dsfr_absolute_lib,
      'favicons'            => $theme_dsfr_lib  .'favicon/',
      'absolute_favicons'   => $theme_dsfr_absolute_lib  .'favicon/',
      'arwork'              => $theme_dsfr_artwork,
      'absolute_arwork'     => $theme_dsfr_absolute_artwork,
      'icons'               => $theme_dsfr_artwork . 'icons/',
      'absolute_icons'      => $theme_dsfr_absolute_artwork . 'icons/',
      'pictograms'          => $theme_dsfr_artwork . 'pictograms/',
      'absolute_pictograms' => $theme_dsfr_absolute_artwork . 'pictograms/',
      'img'                 => $theme_dsfr_dist . 'img/',
      'absolute_img'        => $theme_dsfr_absolute_dist . 'img/',
      'version'             => Tools::getPackageVersion($theme_dsfr_path),
      'setting'             => $theme_dsfr_setting,
      'external'            => $theme_dsfr_setting->get('code.external_lib')
    ];

    // -------------------------------------------------------------------------------- //
    // GET Current Theme Information
    $theme_current = \Drupal::config('system.theme')->get('default');
    $theme_current_name = \Drupal::theme()->getActiveTheme()->getName();
    $theme_current_path = \Drupal::theme()->getActiveTheme()->getPath();
    $theme_current_absolute_path = $root . '/' . $theme_current_path;
    $theme_current_setting = \Drupal::config($theme_current_name . '.settings');

    $current_theme = [
      'default'       => $theme_current, 
      'name'          => $theme_current_name,  
      'dashboard'     => Url::fromRoute('system.theme_settings_theme', ['theme' => $theme_current])->toString(),
      'path'          => '/' . $theme_current_path, 
      'absolute_path' => $theme_current_absolute_path, 
      'external'      => $theme_current_setting->get('code.external_lib'),
      'setting'       => $theme_current_setting
    ]; 

    // -------------------------------------------------------------------------------- //
    // About this DSFR Project
    $distribution = \Drupal::service('extension.list.profile')->getList();

    $dsfr_project = [
      'distrib' => $distribution,
      'kickstart' => (array_key_exists('dsfr_kickstart', $distribution)) ? 1 : 0,      
      'components' => Components::initDsfrComponents($theme_current_setting, $current_theme['external'])
    ];

    // -------------------------------------------------------------------------------- //
    // Final result
    return self::buildFinalResult(
      $global,
      $dsfr_theme,
      $current_theme,
      $dsfr_project,
      'fr-container', // Default container
      self::getBreadcrumbPosition($theme_current_setting)
    );
  }

  /**
   * Get breadcrumb position
   *
   * @param ImmutableConfig $theme_setting
   * @return string
   */
  private static function getBreadcrumbPosition(ImmutableConfig $theme_setting): string {
    
    switch ($theme_setting->get('breadcrumb.position')) {
      case '1': return ' rf-breadcrumb--center';
      case '2': return ' rf-breadcrumb--right';
      default: return '';
    }
  }

  /**
   * Build final result array
   *
   * @param array $global
   * @param array $dsfr_theme
   * @param array $current_theme
   * @param array $dsfr_project
   * @param string $container
   * @param string $breadcrumb_position
   * @return array<string, mixed>
   */
  private static function buildFinalResult(
    array $global,
    array $dsfr_theme,
    array $current_theme,
    array $dsfr_project,
    string $container,
    string $breadcrumb_position
  ): array {

    $theme_setting = $current_theme['setting'];

    $final =  [

      'global'  => $global, // about your project 
      'dsfr'    => $dsfr_theme, // parent or current theme
      'current' => $current_theme, 
      'project' => $dsfr_project, // distrib and components DSFR Drupal

      // Current theme (based of this DSFR Drupal parent theme)

      'general'   => [
        'institution' => ($theme_setting->get('general.institution')) ? nl2br($theme_setting->get('general.institution')) : 'République <br> Française',
        'mourning' => $theme_setting->get('general.mourning'),
        'version' => [
          'name' => $theme_setting->get('general.version.name'),
          'color' => $theme_setting->get('general.version.color'),
          'only' => $theme_setting->get('general.version.only')
        ]
      ],
      'header'    => [
        'container' => ($theme_setting->get('header.container')) ? $container . '-fluid' : $container,
        'logo'    => [
          'img'   => $theme_setting->get('header.logo.img'),
          'text'  => $theme_setting->get('header.logo.text')
        ],
        'settings' => [
          'visible' => $theme_setting->get('header.settings.visible'),
          'label'   => $theme_setting->get('header.settings.label'),
        ],     
        'search' => [
          'label'   => $theme_setting->get('header.search.label'),
        ],     
      ],
      'navigation'  => [
        'container' => ($theme_setting->get('navigation.container')) ? $container . '-fluid' : $container,
      ],
      'hero' => [
        'container' => ($theme_setting->get('hero.container')) ? $container . '-fluid' : $container,
        'margin' => [
          'top'     => $theme_setting->get('hero.margin.top'),
          'bottom'  => $theme_setting->get('hero.margin.bottom')
        ]
      ],
      'breadcrumb' => [
        'position'  => $breadcrumb_position,
        'margin' => [
          'top'     => $theme_setting->get('breadcrumb.margin.top'),
          'bottom'  => $theme_setting->get('breadcrumb.margin.bottom')
        ]
      ],
      'content' => [
        'container' => ($theme_setting->get('content.container')) ? $container . '-fluid' : $container,
        'margin' => [
          'top'     => $theme_setting->get('content.margin.top'),
          'bottom'  => $theme_setting->get('content.margin.bottom')
        ]
      ],
      'footer_top' => [
        'container' => ($theme_setting->get('footer_top.container')) ? $container . '-fluid' : $container,
      ],
      'footer' => [
        'container' => ($theme_setting->get('footer.container')) ? $container . '-fluid' : $container,
        'logo' => [
          'img' => $theme_setting->get('footer.logo.img')
        ],
        'last_line' => $theme_setting->get('footer.last_line')
      ], 
      'code'   => [
        'gdpr' => $theme_setting->get('code.gdpr'),
        'newsletter' => $theme_setting->get('code.newsletter'),
        'w3c' => $theme_setting->get('code.w3c')
      ],
      'features' => [
        'node_user_picture' => $theme_setting->get('features.node_user_picture'),
        'comment_user_picture' => $theme_setting->get('features.comment_user_picture'),
        'comment_user_verification' => $theme_setting->get('features.comment_user_verification'),
        'favicon' => $theme_setting->get('features.favicon')
      ],
      'logo' => [
        'use_default' => $theme_setting->get('logo.use_default'),
        'path'        => $theme_setting->get('logo.path')
      ],
      'favicon' => [
        'use_default' => $theme_setting->get('favicon.use_default'),
        'path'        => $theme_setting->get('favicon.path')
      ]
    ];

    return $final;
  }
}