<?php

namespace Drupal\dsfr\settings;

use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Path\PathMatcherInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Provides utility methods for DSFR theme.
 */
class Tools {

  /**
   * Get a global configuration of the site.
   *
   * @return array{name: string, slogan: string}
   */
  public static function siteConfig(): array {

    /** @var ImmutableConfig $config */
    $config = \Drupal::config('system.site');

    return [
      'name'        => $config->get('name'),
      'slogan'      => $config->get('slogan'),
      'language'    => \Drupal::languageManager()->getCurrentLanguage()->getId(),
      'maintenance' => \Drupal::state()->get('system.maintenance_mode'),
      'users'       => [
        'logged'    => \Drupal::currentUser()->isAuthenticated(),
        'register'  => \Drupal::config('user.settings')->get('register')
      ]
    ];
  }

  /**
   * Get current URL information.
   *
   * @return array{front: bool, current: string, current_clean: string}
   */
  public static function getUrl(): array {

    /** @var CurrentPathStack $pathCurrent */
    $path_current = \Drupal::service('path.current');

    /** @var PathMatcherInterface $pathMatcher */
    $path_matcher = \Drupal::service('path.matcher');
    
    $current = $path_current->getPath();
    $current_clean = str_replace('/', '-', $current);
    $current_clean = substr($current_clean, 1);

    return [
      'front'   => $path_matcher->isFrontPage(),
      'current' => $current,
      'current_clean' => $current_clean
    ];
  }

  /**
   * Provides the version defined in package.json of this DSFR theme.
   *
   * @param string $theme_path The path to the theme directory.
   * @return string|null The version string if found, null otherwise.
   */
  public static function getPackageVersion(string $theme_path): ?string {

    $drupal_root = \Drupal::root();
    $package_json_path = $drupal_root . '/' . $theme_path . '/package.json';
  
    if (file_exists($package_json_path)) {

      $package_json_content = file_get_contents($package_json_path);
      $package_data = json_decode($package_json_content, TRUE);
  
      if (isset($package_data['version'])) {
        return $package_data['version'];
      }
    } 
  
    return NULL;
  }

  /**
   * Translation function to resolve any errors in t() function calls.
   *
   * @param string $string The string to translate.
   * @param array $args An array of replacements to make after translation.
   * @param array $options An array of options for translation.
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   */
  public static function t(string $string, array $args = [], array $options = []): TranslatableMarkup {
    return \Drupal::translation()->translate($string, $args, $options);
  }
}