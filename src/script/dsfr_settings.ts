import '../style/dsfr_settings.css';

const external_lib_1 = document.getElementById('edit-code-external-lib-1') as HTMLInputElement;
const external_lib_0 = document.getElementById('edit-code-external-lib-0') as HTMLInputElement;
const components_lib = document.getElementById('dsfr-components') as HTMLElement;

if (external_lib_1.checked === true) {
  components_lib.style.display = 'none';
}

external_lib_1.addEventListener('click', () => {
  components_lib.style.display = 'none';
});

external_lib_0.addEventListener('click', () => {
  components_lib.style.display = 'grid';
});
