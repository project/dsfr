import type { Config } from 'postcss-load-config';

const config: Config = {
  plugins: {
    "postcss-import": {},
    "postcss-mixins": {},
    "postcss-nested": {},
    "postcss-simple-vars": {},
    "autoprefixer": {},
  }
}

export default config;