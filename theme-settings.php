<?php

/**
 * @file
 * Add custom theme settings to the dsfr base theme.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\dsfr\settings\Components;
use Drupal\dsfr\settings\Form;
use Drupal\dsfr\settings\Theme;
use Drupal\dsfr\settings\Tools;

/**
 * Implements hook_form_system_theme_settings_alter().
 */
function dsfr_form_system_theme_settings_alter(&$form, FormStateInterface $form_state, $form_id = NULL) {
  
  // Preparation of recurrent variables
  $theme = Theme::settings();
  $theme_setting = \Drupal::config( $theme['current']['name'].'.settings' );
  $official = '<a href="https://systeme-de-design.gouv.fr/" target="_blank">'. Tools::t('French State DESIGN SYSTEM') .'</a>';
  $conf = [
    'theme_setting' => $theme_setting,
    'top' => Tools::t('Top margin'),
    'bottom' => Tools::t('Bottom margin'),
    'distance' => 'Distance between ',
    'size' => '<h3> ' . Tools::t('Size') . '</h3>',
    'full' => Tools::t('will take up the FULL WIDTH of the page.')
  ];

  // -------------------------------------------------------------------------------------------------------------- //
  // Adding assets to the configuration page
  $form['#attached']['library'][] = 'dsfr/dsfr_settings';

  // -------------------------------------------------------------------------------------------------------------- //
  // Form initialization
  $form['dsfr'] = array(
    '#title' => Tools::t('Customize the') . ' ' . Tools::t('DSFR Theme'). ' ' . Tools::t('based on') . ' ' . $official,
    '#type' => 'vertical_tabs',
    '#default_tab' => 'edit-dsfr-general',
    '#weight' => -10,
  );

  // -------------------------------------------------------------------------------------------------------------- //
  // DSFR general settings.
  $general = Form::detailsType('General', Tools::t('Contains recurring elements throughout the theme.'));
  $form['general'] = array_merge( $general, general_settings( $conf ));

  // -------------------------------------------------------------------------------------------------------------- //
  // Header tab
  $header = Form::detailsType('Header');
  $form['header'] = array_merge( $header, header_settings( $conf ));

  // -------------------------------------------------------------------------------------------------------------- //
  // Main navigation tab
  $navigation = Form::detailsType('Main menu');
  $container_navigation = Form::checkboxType( Tools::t('The navigation') . ' ' . $conf['full'], 'navigation.container', $theme_setting, $conf['size'] );
  $form['navigation'] = array_merge( $navigation, [ 'container' => $container_navigation ]);

  // -------------------------------------------------------------------------------------------------------------- //
  // Hero (banner) tab
  $hero = Form::detailsType('Hero');
  $form['hero'] = array_merge( $hero, hero_settings( $conf ));

  // -------------------------------------------------------------------------------------------------------------- //
  // Breadcrumb tab
  $breadcrumb = Form::detailsType('Breadcrumb');
  $form['breadcrumb'] = array_merge( $breadcrumb, breadcrumb_settings( $conf ));

  // -------------------------------------------------------------------------------------------------------------- //
  // Content tab
  $content = Form::detailsType('Content');
  $form['content'] = array_merge( $content, content_settings( $conf ));

  // -------------------------------------------------------------------------------------------------------------- //
  // Footer top tab
  $footer_top = Form::detailsType('Footer Top');
  $container_footer_top = Form::checkboxType( Tools::t('The footer top'). ' ' . $conf['full'], 'footer_top.container', $theme_setting, $conf['size']
  );
  $form['footer_top'] = array_merge( $footer_top, [ 'container' => $container_footer_top ]);

  // -------------------------------------------------------------------------------------------------------------- //
  // Footer tab
  $footer = Form::detailsType('Footer');
  $form['footer'] = array_merge( $footer, footer_settings( $conf ));

  // -------------------------------------------------------------------------------------------------------------- //
  // CODE (advanced) tab
  $code = Form::detailsType('Advanced');
  $form['code'] = array_merge( $code, code_settings( $conf ));

}

/**
 * General section
 */
function general_settings( array $conf ): array { $section = 'general'; 

  return [
    'institution' => [
      '#type' => 'textarea',
      '#title' => Tools::t('Name of official (or parent) institution.'),
      '#description' => Tools::t('Mandatory label that will appear <strong>under</strong> the <strong><em>Marianne</em> logo</strong>.')
      .Tools::t('<br>By default or empty, the label will be the example.')
      .Tools::t('<br><strong>Note:</strong> the line crossing will be preserved, but you can add the tag &lt;br&gt; for a new line.'),
      '#default_value' => $conf['theme_setting']->get($section.'.institution'),
      '#attributes' => ['placeholder' => Tools::t('Example') . ':       
République
Française'],
    ],
    'mourning' => Form::checkboxType( Tools::t('Mourning mode'), $section . '.mourning', $conf['theme_setting'] ),
    'version' => [
      'name' => [
        '#type' => 'textfield',
        '#prefix' => '<hr>',
        '#title' => '<h43>'. Tools::t('Version')  .'</h3>' . Tools::t('Name of the version'),
        '#description' => Tools::t('Displays a version badge next to the site title (example: beta, dev, etc.). If the field is not filled in, the badge will not appear.'),
        '#default_value' => $conf['theme_setting']->get($section.'.version.name'),
        '#maxlength' => 30,
        '#attributes' => ['placeholder' => Tools::t('Example') . ': preview'],
      ],
      'color' => [
        '#type' => 'select',
        '#title' => Tools::t('Version badge color'),
        '#default_value' => $conf['theme_setting']->get($section.'.version.color'),
        '#options' => [
          'blue-ecume'=> Tools::t('Blue Ecume'),
          'blue-cumulus' => Tools::t('Blue Cumulus'),
          'green-archipel' => Tools::t('Green Archipel'),
          'green-menthe' => Tools::t('Green Menthe'),
          'green-emeraude' => Tools::t('Green Emeraude'),
          'green-bourgeon' => Tools::t('Green Bourgeon'),
          'green-tilleul-verveine' => Tools::t('Green Tilleul Verveine'),
          'yellow-tournesol' => Tools::t('Yellow Tournesol'),
          'yellow-moutarde' => Tools::t('Yellow Moutarde'),
          'brown-cafe-creme' => Tools::t('Brown Cafe Creme'),
          'brown-caramel' => Tools::t('Brown Caramel'),
          'brown-opera' => Tools::t('Brown Opera'),
          'pink-macaron' => Tools::t('Pink Macaron'),
          'pink-tuile' => Tools::t('Pink Tuile'),
          'purple-glycine' => Tools::t('Purple Glycine'),
          'beige-gris-galet' => Tools::t('Beige Gris Galet'),          
        ]
      ],
      'only' => Form::checkboxType( 
        Tools::t('SHOW ') . ' ' . Tools::t('badge only'), 
        $section . '.version.only', $conf['theme_setting'],
        NULL, Tools::t('Text and image logos will be masked as a priority') 
      )
    ]
  ];
}

/**
 * Header section
 */
function header_settings( array $conf ): array { $section = 'header'; 
  
  return [
    'container' => Form::checkboxType( Tools::t('The header') . ' ' . $conf['full'], $section . '.container', $conf['theme_setting'], $conf['size'] ),
    'logo' => [
      'img' =>  Form::checkboxType( 
        Tools::t('HIDE your personal logo IMAGE') . ' ' . Tools::t('(footer only)'), 
        $section . '.logo.img', $conf['theme_setting'], 
        '<br><hr><h3> ' . Tools::t('Logo') . '</h3>' ),
      'text' => Form::checkboxType( 
        Tools::t('HIDE your personal logo TEXT -name/service-') . ' ' . Tools::t('(header only)'), 
        $section . '.logo.text', $conf['theme_setting'] ),
    ],
    'settings' => [
      'visible' => Form::checkboxType( 
      Tools::t('Activate display settings'), 
      $section . '.settings.visible', $conf['theme_setting'], 
      '<br><hr><h3> ' . Tools::t('Display settings') . '</h3>' ),
      'label' => [
        '#type' => 'textfield',
        '#title' => Tools::t("Display parameters label (also works with the default account menu)"),
        '#description' => Tools::t('If the field is not filled in, the label will be set to '). '«' . Tools::t('Display settings') . '»',
        '#default_value' => $conf['theme_setting']->get($section.'.settings.label'),
        '#maxlength' => 30,
        '#attributes' => ['placeholder' => Tools::t('Example') . ': Display settings'],
      ]
    ],
    'search' => [
      'label' => [
        '#type' => 'textfield',
        '#prefix' => '<hr>',
        '#title' => '<h3>'. Tools::t('Search')  .'</h3>' . Tools::t('Placeholder'),
        '#description' => Tools::t('If the field is not filled in, the label will be set to '). '«' . Tools::t('Search') . '»',
        '#default_value' => $conf['theme_setting']->get($section.'.search.label'),
        '#attributes' => ['placeholder' => Tools::t('Example') . ': Search'],
      ]
    ]
  ];
}

/**
 * Breadcrumb section
 */
function breadcrumb_settings( array $conf ): array { $section = 'breadcrumb';

  return [
    'position' => [
      '#type' => 'select',
      '#prefix' => '<h3> ' . Tools::t('Position') . '</h3>',
      '#description' => Tools::t('The breadcrumb can be positioned horizontally to the left, centre and right...'),
      '#default_value' => $conf['theme_setting']->get( $section.'position' ),
      '#options' => [
        0 => Tools::t('Left'), 
        1 => Tools::t('Center'), 
        2 => Tools::t('Right')
      ]
    ],
    'margin' => Form::marginPattern(
      $section,
      $conf['theme_setting'],
      [ $conf['top'], Tools::t( $conf['distance'].'the header or the hero and the main content.'), 3 ],
      [ $conf['bottom'], Tools::t( $conf['distance'].'the main content and the footer.' ), 3 ]
    )
  ];
}

/**
 * Hero section
 */
function hero_settings( array $conf ): array { $section = 'hero';

  return [
    'container' => Form::checkboxType( Tools::t('The Hero') . ' ' . $conf['full'], $section.'.container', $conf['theme_setting'], $conf['size'] ),
    'margin' => Form::marginPattern(
      $section,
      $conf['theme_setting'],
      [ $conf['top'], Tools::t( $conf['distance'].'the header and the hero.'), 3 ],
      [ $conf['bottom'], Tools::t( $conf['distance'].'the hero and the main content.' ), 3 ]
    )
  ];
}

/**
 * Content section
 */
function content_settings( array $conf ): array { $section = 'content'; 

  return [
    'container' => Form::checkboxType( Tools::t('The main content') . ' ' . $conf['full'], $section . '.container', $conf['theme_setting'], $conf['size']),
    'margin' => Form::marginPattern(
      $section,
      $conf['theme_setting'],
      [ $conf['top'], Tools::t( $conf['distance']. 'the header or the hero and the main content.'), 3 ],
      [  $conf['bottom'], Tools::t( $conf['distance'] . 'the main content and the footer.'), 6 ]
    )
  ];
}

/**
 * Footer section
 */
function footer_settings( array $conf ): array { $section = 'footer'; 

  return [
    'container' => Form::checkboxType( Tools::t('The footer') . ' ' . $conf['full'], $section . '.container', $conf['theme_setting'], $conf['size'] ),
    'logo' => [
      'img' => Form::checkboxType( 
        Tools::t('HIDE your personal logo IMAGE') . ' ' . Tools::t('(footer only)'), 
        $section . '.logo.img', $conf['theme_setting'], 
        '<br><hr><h3> ' . Tools::t('Logo') . '</h3>' )
    ],
    'last_line' => Form::checkboxType( 
      Tools::t('SHOW the line') . ' <strong>«' . Tools::t('Unless otherwise stated, all texts on this site are under')  . ' licence etalab-2.0 »</strong>',
      $section . '.last_line', $conf['theme_setting'],
      '<br><hr><h3> ' . Tools::t('Last line') . '</h3>'
    )
  ];
}

/**
* Advanced section
*/
function code_settings( array $conf ): array { 
  
  $code = [
    'gdpr' => [
      '#type' => 'radios',
      '#title'=> '<h3> ' . Tools::t('GDPR Library') . '</h3>',
      '#description' => '<p class="admin-description">' . Tools::t('Library associated with this component') 
      . ' (' . Tools::t('banner with accept/refuse/customize option') .' + '. Tools::t('Cookies management panel') . ') </p>',
      '#default_value' => $conf['theme_setting']->get('code.gdpr'),    
      '#options' => [
        0   => Tools::t('None'),
        1   => 'Tacjs (' . Tools::t('Module based on Tarteaucitron').')',
        2   => Tools::t('Management proposed by the DSFR library (cookie support code to develop yourself)')
      ]      
    ],
    'newsletter' => [
      '#type' => 'radios',
      '#prefix' => '<div><hr />',
      '#title'=> '<h3> Newsletter (' . Tools::t('Library') . ')</h3>',
      '#suffix' => '</div>',
      '#default_value' => $conf['theme_setting']->get('code.newsletter'),    
      '#options' => [
        0 => Tools::t('None'),
        1 => 'Simplenews',
        2 => 'Other (such as your own code)'
      ]      
    ],
    'w3c' => Form::checkboxType( 
      Tools::t('Remove the role attribute if necessary for W3C validation.'), 
      'code.w3c', $conf['theme_setting'], 
      '<hr /><h3> ' . Tools::t('W3C Validation') . '</h3>',  
      Tools::t('See') . ': <a href="https://validator.w3.org/" target="_blank">Validator W3C</a> '. Tools::t('and') 
      .' <a href="https://www.w3.org/TR/wai-aria-1.0/roles" target="_blank">The Roles Model</a>' 
    ),
    'external_lib' => [
      '#type' => 'radios',
      '#prefix' => '<hr /><h3> ' . Tools::t('Full or partial DSFR loading') . '</h3><div id="dsfr-external-lib">',
      '#suffix' => '</div>',
      '#default_value' => $conf['theme_setting']->get('code.external_lib'),    
      '#options' => [
        1   => Tools::t('Using the library in the library folder') . ' (/libraries/dsfr/dist/dsfr.min.css)' ,
        0   => Tools::t('By default') . ' (' . Tools::t('can be partially loaded') . ')',
      ]
    ]
  ];

  $components = Components::list();
  $n = 0;
  foreach( $components as $component ) {

    $label = ucfirst($component);
    if( $label == 'Utility' ) $label = $label .' (for icons, e.g)';
    $value = 'code.components.'.$component;
    $n++;

    if( $n == 1 ) {
      $code['components'][$component] = [
        '#type' => 'checkbox',
        '#prefix' => '<div id="dsfr-components">',
        '#title' => Tools::t($label),
        '#default_value' => $conf['theme_setting']->get($value),
      ];
    } elseif ( $n == count($components) ) {
      $code['components'][$component] = [
        '#type' => 'checkbox',
        '#suffix' => '</div>',
        '#title' => Tools::t($label),
        '#default_value' => $conf['theme_setting']->get($value),
      ];
    } else {
      $code['components'][$component] = Form::checkboxType( $label, $value, $conf['theme_setting'] );
    }
  }

  return $code;
}