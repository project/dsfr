# DSFR theme documentation for Drupal

Welcome to the official [DSFR Drupal](https://www.drupal.org/project/dsfr) project documentation.

First of all, please select the language version of the documentation:

<a href="en/">
<img src="img/uk.svg" alt="" width="20px" />
English version
</a>


<a href="fr/">
<img src="img/fr.svg" alt="" width="20px" /> Version française
</a>