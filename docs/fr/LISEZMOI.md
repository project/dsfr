# DSFR

DSFR est un thème de base pour Drupal qui utilise pour la parte frontend la bibliothèque DSFR (« Design System FRançais » ou « Système de Design de l'État ») fourni par le Service d’Information du Gouvernement (SIG) français.

Voir: [documentation officielle de la bibliothèque DSFR](https://www.systeme-de-design.gouv.fr/)

## Périmètre d'application

La bibliothèque DSFR doit être utilisée pour les projets numériques au sein de l'État français : administrations centrales, leurs services, délégations interministérielles, préfectures, ambassades et tous les services déconcentrés.

Plus de détails : [Périmètre d'utilisation du DSFR](https://www.systeme-de-design.gouv.fr/comment-utiliser-le-dsfr/perimetre-d-application-du-dsfr)


## Version de la bibliothèque

Elle est indiquée dans le fichier package.json et le fichier dsfr.libraries.yml.

## À propos

Ce thème Drupal et les modules<sup>1</sup> qui gravitent autour de ce projet sont développés par le service interacadémique des systèmes d'information de la région académique Auvergne-Rhône-Alpes (SIASI-AURA). 
Cet écosystème<sup>2</sup> DSFR-Drupal open-source est suivi par la direction interministérielle du numérique (DINUM).

## Paramétrer le thème

Il vous est possible, entre autres, depuis la page "/admin/appearance/settings/dsfr" de :

1. Modifier le nomde l'institution
2. Activer le mode deuil
3. Ajouter un badge (par exemple "preview" avec la couleur de votre choix)
4. Personnaliser le logo (dans l'entête et le pied de page)
5. Gérer les marges des principales régions du thème (entête, pied de page, etc.)
6. Activer la prise en charge de modules optionnels RGPD (Tacjs) et newsletter (Simplenews)
7. Sélectionner les parties de la bibliothèque à charger

## Documentation détaillée

Voir: ./docs/fr/

## Annotations

Distribution et modules experimentaux:
1. Voir : 
* https://www.drupal.org/project/dsfr_twig_components
* https://www.drupal.org/project/dsfr_core
* https://www.drupal.org/project/dsfr_menu
* https://www.drupal.org/project/dsfr_paragraph
2. https://www.drupal.org/project/dsfr_kickstart

