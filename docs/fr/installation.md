# Installer le thème

## Avec drush

```
drush then dsfr -y && drush cset system.theme default dsfr -y
```

## Manuellement

Allez à "/admin/appearance", puis cliquer sur "Installer et définir par défaut" du thème DSFR.