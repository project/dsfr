# Paramètres du thème en français

Si vous souhaitez que les paramètres du thème soient en français, mais que ceux-ci n'ont pas été automatiquement traduits, suivez les intructions ci-dessous.

## Prérequis

1. Le français doit être ajouté à votre projet, soit à l'installation, soit depuis l'addresse : /admin/config/regional/language/add
2. Soit le français est défini par défaut ("/admin/config/regional/language"), soit il est actif selon une détection (par exemple l'url : "/admin/config/regional/language/detection")

## Avec drush

```
drush en config_translation -y
drush locale:check && drush locale:update --langcodes=fr && drush cr
```

## Manuellement

Le module “Configuration Translation” (config_translation) doit être activé (voir "/admin/modules").

Si l'interface n'est pas traduit, allez à "/admin/config/regional/translate/import" puis importer le fichier "[...]/themes/contrib/dsfr/translations/dsfr-fr.po".