# Créer un thème enfant

Il est fortement recommandé de créer un thème enfant du thème DSFR pour votre projet.

## Installation

### Avec bun / npm / yarn

```bash
bun run child
```

Suivez alors les instructions.

Si ça ne fonctionne pas, lire la partie ci-dessous ou procéder à une installation manuelle.

### Avec un script shell

```bash
chmod +x ./shell/create_childtheme.sh && ./shell/create_childtheme.sh
```

Suivez alors les instructions.

> Si les scripts ne fonctionnent pas (par exemple avec WSL), vous pouvez d'abord exécuter l'utilitaire dos2unix pour reformater le fichier
> ```bash
> dos2unix ./scripts/create_childtheme.sh
> ```

Si ça ne fonctionne pas, procéder à une installation manuelle.

### Manuellement

1. Créer si besoin un dossier custom dans le dossier /themes
2. Dans votre dossier themes/custom, placer et renommer une copie du dossier "_dsfr_child"
3. Renommer tous les éléments "_dsfr_child" par le nom système de votre thème custom (noms de fichier et toutes les mentions inclus dans les fichiers)


## Utiliser Vite dans votre thème custom 

Dans le dossier de votre thème enfant :

### Avec bun / npm / yarn
```bash
bun install
```

Puis les commandes comme :
```bash
bun run build
```

Voir les autres commandes disponibles dans le fichier package.json dans votre thème custom (voir aussi ./docs/fr/developpement.md).