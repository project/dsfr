## Contribuer au thème DSFR-Drupal

Ce thème est basé sur le thème parent Stable9 et utilise Vite, Terser et PostCSS pour gérer la partie des fichiers frontend. 

```
dsfr
├─ _dsfr_child (squelette de thème enfant)
├─ config
├─ dist
│  ├─ css
│  ├─ dsfr (bibliothèque officielle DSFR uniquement minifiée)
│  ├─ img
│  └─ js
├─ docs (documentation détaillée)
│  ├─ en
│  │  └─ …
│  └─ fr
│  │  └─ …
├─ public (tout ce qui s’y trouve sera copié pendant la génération du dossier "./dist")
├─ shell (scripts d’automatisation via le shell, comme la création d’un thème enfant)
├─ src
│  ├─ pages (modèles de pages)
│  ├─ script (fichiers additionnels pour le thème en .ts, .js, …)
│  └─ settings (services du thème via des fichiers PHP)
│  └─ style (fichiers additionnels pour le thème en .css, .scss, …) 
├─ templates (fichiers .html.twig)
│  ├─ …
│  ├─ includes
│  │  └─ get-started.html.twig (page personnalisée de bienvenue)
│  ├─ layout (modèles de pages)
│  │  ├─ layout_builder
│  │  ├─ layout_discovery
│  │  ├─ html.html.twig
│  │  ├─ maintenance-page.html.twig
│  │  ├─ page--403--dsfr.html.twig
│  │  ├─ …
│  │  ├─ page.html.twig
│  │  └─ … 
│  ├─ …
│  ├─ navigation
│  └─ views
├─ translations
│  └─ dsfr-fr.po 
├─ .gitignore
├─ .gitlab-cli.yml
├─ dsfr.breakpoints.yml
├─ dsfr.info.yml
├─ dsfr.layouts.yml
├─ dsfr.librairies.yml
├─ dsfr.theme
├─ …
├─ package.json
├─ postcss.config.mts
├─ …
├─ theme-settings.php (charge le formulaire de "/admin/appearance/settings/dsfr")
├─ tsconfig.json
├─ vite.install.ts (nécessaire à la reconstruction et la mise à jour de la bibliothèque DSFR)
└─ vite.config.ts
```

### Commandes de génération du dossier ./dist

Avec bun / npm / yarn :

```
bun install
bun run build
```

#### Pour n'afficher que les messages d'erreur

```
bun run build:silent
```

#### En mode développement (fichiers additionnels non compressés)

```
bun run dev
```

Ou pour voir les changements en temps réel

```
bun run watch
```

### Si vous souhaitez placer la bibliothèque dans vos bibliothèques

Après avoir vérifié que le dossier 'web/libraries' existe :

```
bun run libraries
```


### Obtenir la source du DSFR 

À noter que le processus est assez long. Depuis la racine du  thème DSFR, construire les assets depuis le dépôt :

#### Avec bun / npm / yarn

```
cd src && git clone https://github.com/GouvernementFR/dsfr.git dsfr-repository
cd dsfr-repository && bun install --force
bun run build
cp -R dist ../dsfr && cd ../..
```

Ensuite :
```
bun run install-dsfr
```

Un dossier public/dsfr sera alors présent.