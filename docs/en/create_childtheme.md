# Set up a child theme

We strongly recommend that you create a child theme of the DSFR theme for your project.

## Install

### With bun / npm / yarn

```bash
bun run child
```

Then follow the instructions.

If this doesn't work, read the section below or proceed with a manual installation.

### With a shell script

```bash
chmod +x ./shell/create_childtheme.sh && ./shell/create_childtheme.sh
```

Then follow the instructions.

> If the scripts do not work (e.g. with WSL), you can first run the dos2unix utility to reformat the file
> ```bash
> dos2unix ./scripts/create_childtheme.sh
> ```

If this doesn't work, proceed with a manual installation.

### Manually

1. If necessary, create a custom folder in the /themes folder.
2. In your themes/custom folder, place and rename a copy of the "_dsfr_child" folder.
3. Rename all "_dsfr_child" elements by the system name of your custom theme (filenames and all mentions included in the files).

## Using Vite in your custom theme 

In your child theme folder:

### With bun / npm / yarn

``bash
bun install
```

Then commands like:
``bash
bun run build
```

See the other commands available in the package.json file in your custom theme (see also ./docs/en/developement.md).