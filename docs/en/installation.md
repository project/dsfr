# Installing theme

## With drush

```
drush then dsfr -y && drush cset system.theme default dsfr -y
```

## Manually

Go to “/admin/appearance”, then click on “Install and set as default” for the DSFR theme.