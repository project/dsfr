# Development and contribution

This theme is based on the parent theme Stable9 and uses Vite, Terser and PostCSS to manage the frontend files. 

```
dsfr
├─ _dsfr_child (child theme skeleton)
├─ config
├─ dist
│  ├─ css
│  ├─ dsfr (official DSFR library only minified)
│  ├─ img
│  └─ js
├─ docs (detailed documentation)
│  ├─ en
│  │  └─ …
│  └─ fr
│  │  └─ …
├─ shell (shell-based automation scripts, such as child theme creation)
├─ src
│  ├─ pages (page templates)
│  ├─ public (everything in it will be copied during generation of the “./dist” folder)
│  ├─ script (additional theme files in .ts, .js, …)
│  └─ settings (theme services via PHP files)
│  └─ style (additional theme files in .css, .scss, …)
├─ templates (.html.twig files)
│  ├─ …
│  ├─ includes
│  │  └─ get-started.html.twig (personalized welcome page)
│  ├─ layout (page templates)
│  │  ├─ layout_builder
│  │  ├─ layout_discovery
│  │  ├─ html.html.twig
│  │  ├─ maintenance-page.html.twig
│  │  ├─ page--403--dsfr.html.twig
│  │  ├─ …
│  │  ├─ page.html.twig
│  │  └─ … 
│  ├─ …
│  ├─ navigation
│  └─ views
├─ translations
│  └─ dsfr-fr.po 
├─ .gitignore
├─ .gitlab-cli.yml
├─ dsfr.breakpoints.yml
├─ dsfr.info.yml
├─ dsfr.layouts.yml
├─ dsfr.librairies.yml
├─ dsfr.theme
├─ …
├─ package.json
├─ postcss.config.mts
├─ …
├─ theme-settings.php (loads form from “/admin/appearance/settings/dsfr”)
├─ tsconfig.json
├─ vite.install.ts (required to rebuild and update the DSFR library)
└─ vite.config.ts
```

### Commands for generating ./dist folders

With bun / npm / yarn :

```
bun install
bun run build
```

#### To display only error messages

```
bun run build:silent
```

#### In development mode (additional uncompressed files)

```
bun run dev
```

Or to see changes in real time

```
bun run watch
```

### If you wish to place the library in your libraries

After checking that the 'web/libraries' folder exists :

```
bun run libraries
```

### Get the DSFR source 

Please note that this is a fairly lengthy process. From the DSFR theme root, build assets from the :

```
cd src && git clone https://github.com/GouvernementFR/dsfr.git dsfr-repository
cd dsfr-repository && bun i --force
bun run build
cp -R dist ../dsfr && cd ../..
```

Then :
```
bun run install-dsfr
```

A public/dsfr folder will then be present.