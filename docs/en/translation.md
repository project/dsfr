# Theme settings in French

If you'd like your theme settings to be in French, but they haven't been automatically translated, follow the instructions below.

## Prerequisites

1. French must be added to your project, either during installation or from the address: /admin/config/regional/language/add
2. Either French is defined by default (“/admin/config/regional/language”), or it is activated by detection (e.g. url: “/admin/config/regional/language/detection”).

## With drush

```
drush en config_translation -y
drush locale:check && drush locale:update --langcodes=fr && drush cr
```

## Manually

The “Configuration Translation” module (config_translation) must be enabled (see “/admin/modules”).

If the interface is not translated, go to “/admin/config/regional/translate/import” then import the file “[...]/themes/contrib/dsfr/translations/dsfr-en.po”.