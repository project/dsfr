import { defineConfig } from 'vite';
import { resolve } from 'path';

const assets: string = '';
const script: string = 'src/script/';

export default defineConfig({
  build: {
    minify: 'terser',
    outDir: 'dist',
    emptyOutDir: true,
    sourcemap: false,
    cssCodeSplit: true,
    rollupOptions: {

      input: {

        // Scripts
        _dsfr_child: resolve(__dirname, script+'_dsfr_child.ts')
      },

      output: {
        assetFileNames: (assetInfo) => {
          let extType = assetInfo.name.split('.').at(1);
          if (/\.(png|jpe?g|gif|svg|webp|webm|mp3)$/.test(assetInfo.name)) {
            return `${assets}img/${extType}/[name].${extType}`
          }
          if (/\.(css)$/.test(assetInfo.name)) {
            return `${assets}css/[name].min.${extType}`
          }
          if (/\.(woff|woff2|eot|ttf|otf)$/.test(assetInfo.name)) {
            return `${assets}fonts/[name].min.${extType}`
          }
          return `[name].${extType}`
        },
        chunkFileNames: 'js/[name].min.js',
        entryFileNames: 'js/[name].min.js',
      },
    }
  }
})
