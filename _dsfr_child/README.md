## ASSETS
[Install bun](https://bun.sh/docs/installation) if necessary

```
bun i
bun run build
```

If you want a more Sass-like environment:
```
bun add -D postcss-mixins postcss-simple-vars
```

Then copy the file "postcss.config.mts" from the DSFR theme in this theme.

